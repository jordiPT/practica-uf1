# README #
##Jordi Pagès Triviño##


Sistemes de Gestió Empresarial

### Links ###

* [One Piece](https://www.onepiece.com/en-es/) (Anime)
* [Bring Me The Horizon](https://www.bmthofficial.com/) (Group Band)

### Images ###

* One Piece

![Luffy.png](https://bitbucket.org/repo/9r6GLo/images/3642867588-Luffy.png)

* Bring Me The Horizon

![Bring_Me_The_Horizon.jpg](https://bitbucket.org/repo/9r6GLo/images/4247381366-Bring_Me_The_Horizon.jpg)